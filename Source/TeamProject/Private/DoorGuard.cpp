// Fill out your copyright notice in the Description page of Project Settings.

#include "TeamProject.h"
#include "DoorGuard.h"


ADoorGuard::ADoorGuard(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}

void ADoorGuard::BeginPlay(){
	FirstLocation = GetActorLocation();
}

void ADoorGuard::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
	if (GuardState == EGuardState::CHASE || GuardState == EGuardState::DEAD){

	}
	else{
		Move();
	}
}

void ADoorGuard::Move(){

}

