

#include "TeamProject.h"
#include "ThrowableTracerNode.h"


AThrowableTracerNode::AThrowableTracerNode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	ObjectMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("ThrowableMesh"));
	RootComponent = ObjectMesh;

	// Attribute set in blueprint
	// Transparent material set in blueprint construction script
	// Material: m_ThrowableTracerNode
}


