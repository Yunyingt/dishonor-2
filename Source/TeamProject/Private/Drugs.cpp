

#include "TeamProject.h"
#include "Drugs.h"


ADrugs::ADrugs(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	Time = 10.0f;

	DrugMesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("KeyMesh"));
	RootComponent = DrugMesh;

	TriggerVolume = PCIP.CreateDefaultSubobject<UBoxComponent>(this, TEXT("TriggerVolume"));
	TriggerVolume->AttachTo(RootComponent);

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ADrugs::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
}

void ADrugs::OnUse(){

}


