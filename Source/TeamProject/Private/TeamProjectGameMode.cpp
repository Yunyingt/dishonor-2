// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "TeamProject.h"
#include "TeamProjectGameMode.h"
#include "TeamProjectHUD.h"
#include "TeamProjectCharacter.h"

ATeamProjectGameMode::ATeamProjectGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/MyCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATeamProjectHUD::StaticClass();
}
