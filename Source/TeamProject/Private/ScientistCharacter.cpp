

#include "TeamProject.h"
#include "ScientistCharacter.h"
#include "AIScientistController.h"
#include "TeamProjectCharacter.h"
#include "Particles/ParticleSystemComponent.h"

AScientistCharacter::AScientistCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	AIControllerClass = AAIScientistController::StaticClass();

	DetectCharacter = false;

	//true when actually punching in attack
	isAttacking = false;
	//CharacterMovement->MaxWalkSpeed = 300;
}

void AScientistCharacter::BeginPlay()
{

	Super::BeginPlay();
	UWorld* World = GetWorld();
	if (World)
	{
		CharacterMovement->MaxWalkSpeed = WalkSpeed;
		//get player pawn
		APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(World, 0);
		if (PlayerPawn)
		{
			//
			//SetActorLocation(PlayerPawn->GetActorLocation());
			//MoveToActor(PlayerPawn);
		}
	}

}

bool AScientistCharacter::SightlineDetection(){
	static FName WeaponFireTag = FName(TEXT("Guard Sightline"));
	FVector StartPos = GetActorLocation();
	StartPos.Z = StartPos.Z - 50.0f;
	FVector EndPos = StartPos + 300.0f*GetActorForwardVector();

	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	//This fires the ray and checks against all objects w/ collision
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingle(Hit, StartPos, EndPos, TraceParams, FCollisionObjectQueryParams::AllObjects);

	if (Hit.bBlockingHit){
		//PlayHitEffect(HitEF, Hit.Location);
		AActor* a = Hit.GetActor();
		if (a)
		{
			if (a->IsA<ATeamProjectCharacter>()){
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Sightline: Hit Player Character"));
				return true;
			}
			else{
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Sightline: Blocked by object. Not seeing player character"));
				return false;
			}
		}
		else
		{
			return false;
		}

	}
	else{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, TEXT("Sightline: Doesn't hit anything but the player is within the range"));
		return DetectCharacter;
	}


}

void AScientistCharacter::HearBottle(FVector Location)
{
	AAIScientistController* MyController = Cast<AAIScientistController>(GetController());
	MyController->HearBottle(Location);
}

//spawn explosion at explosionLocation called by AI
void AScientistCharacter::Detonate()
{
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionActor, ExplosionLocation->GetActorLocation());
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionCue, ExplosionLocation->GetActorLocation());
	
	//if player is in explosion radius, kill player
	if (PlayerPawn->GetDistanceTo(this) < ExplosionRadius)
	{
		ATeamProjectCharacter* PlayerCharacter = Cast<ATeamProjectCharacter>(PlayerPawn);
		PlayerCharacter->Kill();
	}
	Controller->UnPossess();
	this->Destroy();

}