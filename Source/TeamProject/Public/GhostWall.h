

#pragma once

#include "GameFramework/Actor.h"
#include "GhostWall.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AGhostWall : public AActor
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Ghost)
	TSubobjectPtr<UStaticMeshComponent> WallMesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Ghost)
	TSubobjectPtr<UBoxComponent> TriggerVolume;

	UFUNCTION(BlueprintCallable, Category = Ghost)
	void OnComponentBeginOverlap(AActor* a);

	UFUNCTION(BlueprintCallable, Category = Ghost)
	void OnComponentEndOverlap(AActor* a);

	void StopCollision();

	virtual void Tick(float DeltaSeconds);
	
};
