

#pragma once

#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	//void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = Detection)
	float WalkSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Detection)
	float RunSpeed;
	UPROPERTY(EditDefaultsOnly, Category = Detection)
		float SoundRadius;
	UPROPERTY(EditDefaultsOnly, Category = Detection)
		float AttackRange;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* AttackAnim;
	bool isAttacking;


	//points to patrol through
	UPROPERTY(EditAnywhere, Category = Patrol)
		TArray<AActor*> TargetPoints;

	//switches from walk to run and vice versa
	void ToggleMoveSpeedWalk();
	void ToggleMoveSpeedRun();

	void HearBottle(FVector Location);

public:

	TArray<AActor*> GetPatrolPoints();

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Guard)
		bool DetectCharacter;
	
	UFUNCTION(BlueprintCallable, Category = Guard)
		float PlayAttack();
};
