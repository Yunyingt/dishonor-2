

#pragma once

#include "AIEnemyController.h"
#include "ScientistCharacter.h"
#include "AIScientistController.generated.h"


/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AAIScientistController : public AAIEnemyController
{
	GENERATED_UCLASS_BODY()

	void BeginPlay() override;
	void Tick(float deltaTime) override;
	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;

	//called when scientist is at self destruct button, sends message to incinerate room
	UFUNCTION(BlueprintCallable, Category = Patrol)
	void PressSelfDestructButton();

	AScientistCharacter* MyCharacter;

	void StateChange();

	UFUNCTION(BlueprintCallable, Category = Patrol)
		void NextPatrolPoint();

	UFUNCTION(BlueprintCallable, Category = Patrol)
		void ChangeStatePatrol();



	//location of my self destruct button
	UPROPERTY(EditAnywhere, Category = science)
	AActor* SDButton;

	//called by GuardCharacter from Throwable right as the Throwable is destroyed
	//UFUNCTION(BlueprintCallable, Category = Patrol)
	//void HearBottle(FVector Location);
	
};
