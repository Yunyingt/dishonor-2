

#pragma once

#include "Drugs.h"
#include "GhostDrug.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AGhostDrug : public ADrugs
{
	GENERATED_UCLASS_BODY()

	//HACK to make player look at actor behind them
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "jumpscare")
	AActor* JumpScareActor;

	virtual void OnUse() override;
	
};
