

#pragma once

#include "Drugs.h"
#include "InvisibleDrug.generated.h"

/**
 * 
 */
UCLASS()
class TEAMPROJECT_API AInvisibleDrug : public ADrugs
{
	GENERATED_UCLASS_BODY()


	void OnUse();

	//HACK to make player look at actor behind them
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "jumpscare")
	AActor* JumpScareActor;

	// Method
	void Tick(float DeltaSeconds) override;
	
};
