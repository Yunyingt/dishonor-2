// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

#ifdef TEAMPROJECT_TeamProjectProjectile_generated_h
#error "TeamProjectProjectile.generated.h already included, missing '#pragma once' in TeamProjectProjectile.h"
#endif
#define TEAMPROJECT_TeamProjectProjectile_generated_h

#define ATeamProjectProjectile_EVENTPARMS
#define ATeamProjectProjectile_RPC_WRAPPERS \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(AActor,OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,OtherComp); \
		P_GET_STRUCT(FVector,NormalImpulse); \
		P_GET_STRUCT_REF(struct FHitResult,Out_Hit); \
		P_FINISH; \
		this->OnHit(OtherActor,OtherComp,NormalImpulse,Out_Hit); \
	}


#define ATeamProjectProjectile_CALLBACK_WRAPPERS
#define ATeamProjectProjectile_INCLASS \
	private: \
	static void StaticRegisterNativesATeamProjectProjectile(); \
	friend TEAMPROJECT_API class UClass* Z_Construct_UClass_ATeamProjectProjectile(); \
	public: \
	DECLARE_CLASS(ATeamProjectProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TeamProject, NO_API) \
	/** Standard constructor, called after all reflected properties have been initialized */    NO_API ATeamProjectProjectile(const class FPostConstructInitializeProperties& PCIP); \
	DECLARE_SERIALIZER(ATeamProjectProjectile) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#undef UCLASS_CURRENT_FILE_NAME
#define UCLASS_CURRENT_FILE_NAME ATeamProjectProjectile


#undef UCLASS
#undef UINTERFACE
#define UCLASS(...) \
ATeamProjectProjectile_EVENTPARMS


#undef GENERATED_UCLASS_BODY
#undef GENERATED_IINTERFACE_BODY
#define GENERATED_UCLASS_BODY() \
public: \
	ATeamProjectProjectile_RPC_WRAPPERS \
	ATeamProjectProjectile_CALLBACK_WRAPPERS \
	ATeamProjectProjectile_INCLASS \
public:


