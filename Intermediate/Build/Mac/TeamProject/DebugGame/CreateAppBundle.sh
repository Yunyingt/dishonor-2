#!/bin/sh
cd "/Users/Pllx/Documents/Unreal Projects/TeamProject/Binaries/Mac"
mkdir -p "TeamProject-Mac-DebugGame.app/Contents/MacOS"
mkdir -p "TeamProject-Mac-DebugGame.app/Contents/Resources/RadioEffectUnit.component/Contents/MacOS"
mkdir -p "TeamProject-Mac-DebugGame.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources/English.lproj"
sh "/Users/Pllx/Documents/Unreal Projects/TeamProject/Intermediate/Build/Mac/TeamProject/DebugGame/DylibCopy.sh" "TeamProject-Mac-DebugGame"
cp -f "/Users/Shared/UnrealEngine/4.4/Engine/Source/Runtime/Launch/Resources/Mac/UE4.icns" "TeamProject-Mac-DebugGame.app/Contents/Resources/UE4.icns"
cp -f "/Users/Shared/UnrealEngine/4.4/Engine/Source/Runtime/Launch/Resources/Mac/Info.plist" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
sed -i "" "s/\${EXECUTABLE_NAME}/TeamProject-Mac-DebugGame/g" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
sed -i "" "s/\${APP_NAME}/TeamProject/g" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
sed -i "" "s/\${MACOSX_DEPLOYMENT_TARGET}/10.9.2/g" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
sed -i "" "s/\${ICON_NAME}/UE4/g" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
sed -i "" "s/\${BUNDLE_VERSION}/4.4.1/g" "TeamProject-Mac-DebugGame.app/Contents/Info.plist"
echo 'echo -n "APPL????"' | bash > "TeamProject-Mac-DebugGame.app/Contents/PkgInfo"
cp -f "/Users/Shared/UnrealEngine/4.4/Engine/Source/ThirdParty/Mac/RadioEffectUnit/RadioEffectUnit.component/Contents/MacOS/RadioEffectUnit" "TeamProject-Mac-DebugGame.app/Contents/Resources/RadioEffectUnit.component/Contents/MacOS/RadioEffectUnit"
cp -f "/Users/Shared/UnrealEngine/4.4/Engine/Source/ThirdParty/Mac/RadioEffectUnit/RadioEffectUnit.component/Contents/Resources/English.lproj/Localizable.strings" "TeamProject-Mac-DebugGame.app/Contents/Resources/RadioEffectUnit.component/Contents/Resources/English.lproj/Localizable.strings"
cp -f "/Users/Shared/UnrealEngine/4.4/Engine/Source/ThirdParty/Mac/RadioEffectUnit/RadioEffectUnit.component/Contents/Info.plist" "TeamProject-Mac-DebugGame.app/Contents/Resources/RadioEffectUnit.component/Contents/Info.plist"
touch -c "TeamProject-Mac-DebugGame.app"
